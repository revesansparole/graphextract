"""
Categorical data
================

Extract and plot categorical data.
"""
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import pandas as pd

from graphextract.svg_extractor import extract_data

data = extract_data("fig3.svg", x_formatter=str, y_formatter=int, x_type='bin')
(vname, pts), = data.items()

df = pd.DataFrame({
    'tree': [f'fuji_{lab}' for lab, _ in pts],
    vname: [val for _, val in pts]
}).set_index(['tree'])

# plot result
fig, axes = plt.subplots(1, 2, figsize=(10, 6), squeeze=False)
ax = axes[0, 0]
ax.bar(df.index, df['nb_fruit'])

ax.set_ylabel("fruit number")
ax.set_ylim(0, 150)
ax.set_yticks([0, 50, 100])

ax = axes[0, 1]
img = mpimg.imread("fig3.png")
ax.imshow(img)
ax.set_xticks([])
ax.set_yticks([])

fig.tight_layout()
plt.show()
