"""
Linear data
===========

Extract and plot categorical data.
"""
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from graphextract.svg_extractor import extract_data

data = extract_data("fig4.svg", x_formatter=int, y_formatter=int)

# plot result
fig, axes = plt.subplots(1, 2, figsize=(12, 6), squeeze=False)
ax = axes[0, 0]
for temp, pts in data.items():
    pts.sort()
    ax.plot([ppfd for ppfd, _ in pts], [an for _, an in pts], '-o', label=temp)

ax.legend(loc='lower right', title='leaf temperature')
ax.set_ylabel("assimilation")
ax.set_ylim(-5, 20)
ax.set_yticks(range(-5, 21, 5))

ax.set_xlabel("PFD")
ax.set_xlim(-200, 2200)
ax.set_xticks(range(0, 2200, 500))

ax = axes[0, 1]
img = mpimg.imread("fig4.png")
ax.imshow(img)
ax.set_xticks([])
ax.set_yticks([])

fig.tight_layout()
plt.show()
