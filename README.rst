========================
graphextract
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/graphextract/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/graphextract/1.2.0/

.. image:: https://revesansparole.gitlab.io/graphextract/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/graphextract

.. image:: https://revesansparole.gitlab.io/graphextract/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/graphextract/

.. image:: https://badge.fury.io/py/graphextract.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/graphextract

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/graphextract/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/graphextract/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/graphextract/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/graphextract/commits/main
.. #}

Extract graph from image

